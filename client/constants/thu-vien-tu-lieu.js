const references = {
    'duc-cham': 'Đục chạm',
    'hoan-thanh': 'Hoàn thành',
    'lap-dung': 'Lắp dựng',
    'nhap-go': 'Nhập gỗ',
    'sam-go': 'Sàm gỗ',
    'van-chuyen-den-cong-trinh': 'Vận chuyển đến công trình',
};

export default references;
