const projects = {
    'nha-5-gian': 'Nhà 5 gian',
    'nha-3-gian': 'Nhà 3 gian',
    'nha-8-mai-co-dien': 'Nhà 8 mái cổ điển',
    'nha-cau': 'Nhà cầu',
    'nha-luc-giac': 'Nhà lục giác',
    'noi-that': 'Nội thất',
    'cong-go': 'Cổng gỗ',
    'do-tho': 'Đồ thờ',
};

export default projects;
